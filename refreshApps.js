const appStore = require('./scripts/helperAppStore.js')
const playStore = require('./scripts/helperPlayStore.js')

appStore.refreshAll()
playStore.refreshAll()
