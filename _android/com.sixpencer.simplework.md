---
wsId: dfox
title: "Dfox-Crypto Wallet and DeFi Portfolio"
altTitle: 
authors:
- leo
users: 1000
appId: com.sixpencer.simplework
launchDate: 
latestUpdate: 2021-03-12
apkVersionName: "1.3.5"
stars: 4.8
ratings: 79
reviews: 40
size: 13M
website: https://dfox.cc
repository: 
issue: 
icon: com.sixpencer.simplework.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app appears not to get access to spend your Bitcoins:

> Dfox is a chain-agnostic crypto portfolio tracker.
