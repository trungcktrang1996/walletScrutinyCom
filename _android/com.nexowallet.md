---
wsId: nexo
title: "Nexo - Crypto Banking Account"
altTitle: 
authors:
- leo
users: 500000
appId: com.nexowallet
launchDate: 
latestUpdate: 2021-03-23
apkVersionName: "1.4.3"
stars: 3.8
ratings: 7138
reviews: 3416
size: 61M
website: https://nexo.io
repository: 
issue: 
icon: com.nexowallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: NexoFinance
providerLinkedIn: 
providerFacebook: nexofinance
providerReddit: Nexo

redirect_from:
  - /com.nexowallet/
---


In the description on Google Play we read:

> • 100% Secured by Leading Audited Custodian BitGo

which makes it a custodial app. The custodian is claimed to be "BitGo" so as a
user you have to trust BitGo to not lose the coins and Nexo to actually not hold
all or part of the coins. In any case this app is **not verifiable**.
