---
wsId: Remitano
title: "Remitano - Buy & Sell Bitcoin Fast & Securely"
altTitle: 
authors:
- leo
users: 100000
appId: com.remitano.remitano
launchDate: 
latestUpdate: 2021-03-18
apkVersionName: "5.20.0"
stars: 4.1
ratings: 11156
reviews: 5279
size: 23M
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: company/Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:
  - /posts/com.remitano.remitano/
---


This app is an interface to an exchange which holds your coins. On Google Play
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.
