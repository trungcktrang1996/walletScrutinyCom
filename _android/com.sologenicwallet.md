---
wsId: 
title: "SOLO Wallet"
altTitle: 
authors:

users: 5000
appId: com.sologenicwallet
launchDate: 
latestUpdate: 2020-06-23
apkVersionName: "1.4.3"
stars: 3.6
ratings: 54
reviews: 36
size: 28M
website: 
repository: 
issue: 
icon: com.sologenicwallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.sologenicwallet/
  - /posts/com.sologenicwallet/
---


This wallet does not support BTC.
