---
wsId: 
title: "Atomex - Crypto Wallet & Atomic swap DEX"
altTitle: 
authors:

users: 1000
appId: com.atomex.android
launchDate: 
latestUpdate: 2021-02-25
apkVersionName: "1.0"
stars: 4.9
ratings: 26
reviews: 26
size: 47M
website: 
repository: 
issue: 
icon: com.atomex.android.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/177 -->
