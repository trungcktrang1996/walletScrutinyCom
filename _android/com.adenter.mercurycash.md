---
wsId: mercurycash
title: "Mercury Cash"
altTitle: 
authors:
- leo
users: 10000
appId: com.adenter.mercurycash
launchDate: 
latestUpdate: 2021-03-15
apkVersionName: "5.0.2"
stars: 4.0
ratings: 181
reviews: 129
size: 82M
website: http://mercury.cash
repository: 
issue: 
icon: com.adenter.mercurycash.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-08-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: mercurycash
providerLinkedIn: 
providerFacebook: mercurycash
providerReddit: 

redirect_from:
  - /com.adenter.mercurycash/
---


This app makes no claims about self-custody so we have to assume it is a
custodial product and thus **not verifiable**.
