---
wsId: CoinbaseWallet
title: "Coinbase Wallet"
altTitle: 
authors:
- leo
appId: org.toshi.distribution
appCountry: 
idd: 1278383455
released: 2017-09-27
updated: 2021-03-18
version: "24.0"
score: 4.6617
reviews: 22533
size: 138215424
developerWebsite: https://wallet.coinbase.com
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinbaseWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of the
[Android Coinbase Wallet — Crypto Wallet & DApp Browser](/android/org.toshi).

Just like the Android version, this wallet is **not verifiable**.
