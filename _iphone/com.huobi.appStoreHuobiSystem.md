---
wsId: huobi
title: "Huobi - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2021-03-19
version: "6.2.1"
score: 4.78949
reviews: 2874
size: 213311488
developerWebsite: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

