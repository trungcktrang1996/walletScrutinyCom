---
wsId: dfox
title: "Dfox - Wallet&DeFi Portfolio"
altTitle: 
authors:
- leo
appId: com.sixpencer.simplework
appCountry: 
idd: 1529717509
released: 2020-10-24
updated: 2021-03-14
version: "1.3.5"
score: 4.71429
reviews: 7
size: 51124224
developerWebsite: https://dfox.cc
repository: 
issue: 
icon: com.sixpencer.simplework.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app appears not to get access to spend your Bitcoins:

> Dfox is a chain-agnostic crypto portfolio tracker.
