---
wsId: STASISStablecoinWallet
title: "STASIS Stablecoin Wallet"
altTitle: 
authors:
- leo
appId: net.stasis.mobile
appCountry: 
idd: 1371949230
released: 2018-07-06
updated: 2021-02-09
version: "7.10"
score: 3.66667
reviews: 3
size: 22039552
developerWebsite: https://stasis.net
repository: 
issue: 
icon: net.stasis.mobile.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

