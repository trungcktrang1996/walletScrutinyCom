---
wsId: jaxxliberty
title: "Jaxx Liberty Blockchain Wallet"
altTitle: 
authors:
- leo
appId: com.liberty.jaxx
appCountry: 
idd: 1435383184
released: 2018-10-03
updated: 2021-03-02
version: "2.6.1"
score: 4.5025
reviews: 1198
size: 46631936
developerWebsite: https://jaxx.io
repository: 
issue: 
icon: com.liberty.jaxx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

