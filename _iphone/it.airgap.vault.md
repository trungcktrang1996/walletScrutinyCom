---
wsId: AirGapVault
title: "AirGap Vault - Secure Secrets"
altTitle: 
authors:
- leo
appId: it.airgap.vault
appCountry: 
idd: 1417126841
released: 2018-08-24
updated: 2021-03-01
version: "3.6.2"
score: 5
reviews: 1
size: 29917184
developerWebsite: 
repository: 
issue: 
icon: it.airgap.vault.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---

