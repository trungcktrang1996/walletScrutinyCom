---
wsId: crypterium
title: "Crypterium | Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.Crypterium.Crypterium
appCountry: 
idd: 1360632912
released: 2018-03-26
updated: 2021-03-18
version: "1.15.0"
score: 4.50155
reviews: 969
size: 253037568
developerWebsite: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

