---
wsId: bluewallet
title: "BlueWallet - Bitcoin wallet"
altTitle: 
authors:
- leo
appId: io.bluewallet.bluewallet
appCountry: 
idd: 1376878040
released: 2018-05-27
updated: 2021-03-02
version: "6.0.6"
score: 4.21026
reviews: 195
size: 66043904
developerWebsite: 
repository: 
issue: 
icon: io.bluewallet.bluewallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

