---
wsId: UpholdbuyandsellBitcoin
title: "Uphold: buy and sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.uphold.wallet.ios
appCountry: 
idd: 1101145849
released: 2016-04-19
updated: 2021-03-17
version: "4.15.14"
score: 3.9814
reviews: 3870
size: 65196032
developerWebsite: 
repository: 
issue: 
icon: com.uphold.wallet.ios.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

